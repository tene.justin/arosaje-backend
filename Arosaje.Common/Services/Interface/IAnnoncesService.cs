﻿using Arosaje.DAL.Models;
using Arosaje.Dtos;
using Microsoft.AspNetCore.Http;

namespace Arosaje.Common.Services.Interface
{
    public interface IAnnoncesService
    {
        List<byte[]> ConvertToByte(List<IFormFile> files);

        Annonces ConvertToAnnonces(AnnonceDto annonceDto, string filename);
    }
}
