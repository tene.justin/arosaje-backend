﻿using Arosaje.Common.Services;
using Arosaje.Common.Services.Interface;
using Arosaje.DAL.Repository;
using Arosaje.DAL.Repository.Interface;

namespace Arosaje.API.Configuration
{
    public static class IoCConfiguration
    {
        public static void ConfigureScopeService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAnnoncesRepository, AnnoncesRepository>();
            services.AddScoped<IAnnoncesService, AnnoncesService>();
        }
    }
}
