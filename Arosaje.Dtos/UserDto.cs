﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arosaje.Dtos
{
    public class UserDto
    {
        public string Identifiant { get; set; }
        public string Password { get; set; }
    }
}
