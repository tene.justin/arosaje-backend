﻿using Arosaje.DAL.Models;

namespace Arosaje.DAL.Repository.Interface
{
    public interface IUserRepository
    {
        User GetUserFromCred(string username, string password);

        public void InsertUser(User user);

        User GetUserById(int id);
    }
}
