﻿using Arosaje.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Arosaje.DAL.Configuration
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<Annonces> Annonces { get; set; }
        public DbSet<User> User { get; set; }
    }
}
