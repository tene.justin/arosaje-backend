﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Arosaje.DAL.Migrations
{
    /// <inheritdoc />
    public partial class InitSqlServer : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Annonces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titre = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Description = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    ImageName = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Ville = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Latitude = table.Column<double>(type: "float", nullable: false),
                    Longitude = table.Column<double>(type: "float", nullable: false),
                    DateCreation = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateModification = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsAvailable = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Annonces", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Prenom = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Pseudo = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Password = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Email = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Sexe = table.Column<string>(type: "varchar(1)", nullable: false),
                    DateDeNaissance = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    IsBotanist = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Annonces");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
