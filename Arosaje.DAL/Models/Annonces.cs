﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arosaje.DAL.Models
{
    public class Annonces
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Titre { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Description { get; set; }

        public double Price { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string ImageName { get; set; }

        public int UserId { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Ville { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public DateTime DateCreation { get; set; } = DateTime.Now;

        public DateTime DateModification { get; set; } = DateTime.Now;

        public bool IsAvailable { get; set; }

    }
}
