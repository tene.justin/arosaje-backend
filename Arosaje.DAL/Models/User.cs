﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arosaje.DAL.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Nom { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Prenom { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Pseudo { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string Password { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string Email { get; set; }

        [Column(TypeName = "varchar(1)")]
        public string Sexe { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public DateOnly DateDeNaissance { get; set; }

        public bool IsBotanist { get; set; }
    }
}
